﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Graficas
{
    class Funcion
    {
        public int alfa;
        public int beta;
        public int gamma;
        public int time = 0;
        public Funcion(int alfa, int beta, int gamma)
        {
            this.alfa = alfa;
            this.beta = beta;
            this.gamma = gamma;
        }
        public double Graficar(double time)
        {
            double y=0;
            double x;
            if (time < alfa)
            {
                y = 0;
            }
            else if (alfa <= time && time <= beta)
            {
                y = (time - alfa) / (beta - alfa);
            }
            else if (beta <= time && time <= gamma)
            {
                y = (gamma - time) / (gamma - beta);
            }
            else if (time > gamma)
            {
                y = 0;
            }
            return y;
        }

        #region
        //public double Graficar2(double time)
        //{
        //    double y = 0;
        //    double x;
        //    if (time < alfa)
        //    {
        //        y = 1;
        //    }
        //    else if (alfa <= time && time <= beta)
        //    {
        //        y = 1 - (time - alfa) / (beta - alfa);
        //    }
        //    else if (beta <= time && time <= gamma)
        //    {
        //        y = 1 - (gamma - time) / (gamma - beta);
        //    }
        //    else if (time > gamma)
        //    {
        //        y = 1;
        //    }
        //    return y;
        //}
        #endregion
    }
}