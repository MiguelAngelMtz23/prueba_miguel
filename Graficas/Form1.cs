﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Graficas
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        Funcion p1 = new Funcion(0, 5, 10);
        Funcion p2 = new Funcion(5, 10, 15);
        Funcion p3 = new Funcion(10, 15, 20);
        Funcion p4 = new Funcion(15, 20, 25);
        Funcion p5 = new Funcion(20, 25, 30);

        #region Empezar
        private void button1_Click(object sender, EventArgs e)
        {
            /*
            for (int i = 0; i <= 30; i++)
            {

                chart1.Series[0].Points.AddXY(i, p1.Graficar(i));
                chart1.Series[1].Points.AddXY(i, p2.Graficar(i));
                chart1.Series[2].Points.AddXY(i, p3.Graficar(i));
                chart1.Series[3].Points.AddXY(i, p4.Graficar(i));
                //chart1.Series[4].Points.AddXY(i, p5.Graficar(i));

                chart2.Series[0].Points.AddXY(i, p1.Graficar(i));
                chart2.Series[1].Points.AddXY(i, p2.Graficar(i));
                chart2.Series[2].Points.AddXY(i, p3.Graficar(i));
                chart2.Series[3].Points.AddXY(i, p4.Graficar(i));
                //chart2.Series[4].Points.AddXY(i, p5.Graficar(i));

                chart3.Series[0].Points.AddXY(i, p1.Graficar(i));
                chart3.Series[1].Points.AddXY(i, p2.Graficar(i));
                chart3.Series[2].Points.AddXY(i, p3.Graficar(i));
                chart3.Series[3].Points.AddXY(i, p4.Graficar(i));
                //chart3.Series[4].Points.AddXY(i, p4.Graficar(i));
            }*/
        }

        #endregion Empezar

        #region Button2
        private void Button2_Click(object sender, EventArgs e)
        {
            //int alfa = 5, beta = 10, gamma = 15, time = 0;
            //int x, y = 0;
            //for (int i = 0; i <= 20; i++)
            //{
            //    if (time < alfa)
            //    {
            //        y = 1;
            //    }

            //    else if (alfa <= time && time <= beta)
            //    {
            //        y = 1 - (time - alfa) / (beta - alfa);
            //    }
            //    else if (beta <= time && time <= gamma)
            //    {
            //        y = 1 - (gamma - time) / (gamma - beta);
            //    }
            //    else if (time > gamma)
            //    {
            //        y = 1;
            //    }

            //    time++;

            //    chart1.Series["Series2"].Points.AddY(y);
            //    chart2.Series["Series2"].Points.AddY(y);
            //    chart3.Series["Series2"].Points.AddY(y);


            //}
        }
        #endregion Button2

        #region Cerrar
        private void button3_Click_1(object sender, EventArgs e)
        {
            Application.Exit();
        }
        #endregion Cerrar

        #region Form1
        private void Form1_Load(object sender, EventArgs e)
        {
            for (int i = 0; i <= 30; i++)
            {

                chart1.Series[0].Points.AddXY(i, p1.Graficar(i));
                chart1.Series[1].Points.AddXY(i, p2.Graficar(i));
                chart1.Series[2].Points.AddXY(i, p3.Graficar(i));
                chart1.Series[3].Points.AddXY(i, p4.Graficar(i));
                //chart1.Series[4].Points.AddXY(i, p5.Graficar(i));

                chart2.Series[0].Points.AddXY(i, p1.Graficar(i));
                chart2.Series[1].Points.AddXY(i, p2.Graficar(i));
                chart2.Series[2].Points.AddXY(i, p3.Graficar(i));
                chart2.Series[3].Points.AddXY(i, p4.Graficar(i));
                //chart2.Series[4].Points.AddXY(i, p5.Graficar(i));

                chart3.Series[0].Points.AddXY(i, p1.Graficar(i));
                chart3.Series[1].Points.AddXY(i, p2.Graficar(i));
                chart3.Series[2].Points.AddXY(i, p3.Graficar(i));
                chart3.Series[3].Points.AddXY(i, p4.Graficar(i));
                //chart3.Series[4].Points.AddXY(i, p4.Graficar(i));
                
            }
            
            if (txtTemperatura.ToString() =="Baja" && txtTemperatura.ToString() == "Poca" && txtViento.ToString() == "Lenta")
            {
                textBox1.Text = "Clima Podria Llover";
            }



        }
        #endregion Form1

        #region ScrollBar1_Scroll
        private void hScrollBar1_Scroll(object sender, ScrollEventArgs e)
        {
            chart1.Series[4].Points.Clear();

            double x = double.Parse(hScrollBar1.Value.ToString());
            double[] Y = new double[4];
            Y[0] = p1.Graficar(x);
            Y[1] = p2.Graficar(x);
            Y[2] = p3.Graficar(x);
            Y[3] = p4.Graficar(x);
            String[] linguistica = {"Baja", "Normal", "Media", "Alta" };

            double Max = Y.Max();

            for (int i = 0; i < Y.Length; i++)
            {
                if (Y[i] == Max)
                {
                    txtTemperatura.Text = linguistica[i];
                }
            }
            chart1.Series[4].Points.AddXY(hScrollBar1.Value, 0);
            chart1.Series[4].Points.AddXY(hScrollBar1.Value, 1);
        }
        #endregion ScrollBar1_Scroll

        #region chart1
        private void chart1_Click(object sender, EventArgs e)
        {

        }
        #endregion chart1

        #region chart2
        private void chart2_Click(object sender, EventArgs e)
        {

        }
        #endregion chart2

        #region ScrollBar2_Scroll
        private void hScrollBar2_Scroll(object sender, ScrollEventArgs e)
        {
            chart2.Series[4].Points.Clear();

            double x = double.Parse(hScrollBar2.Value.ToString());
            double[] Y = new double[4];
            Y[0] = p1.Graficar(x);
            Y[1] = p2.Graficar(x);
            Y[2] = p3.Graficar(x);
            Y[3] = p4.Graficar(x);

            String[] linguistica = {"Poca", "Moderada", "Normal","Mucha" };

            double Max = Y.Max();

            for (int i = 0; i < Y.Length; i++)
            {
                if (Y[i] == Max)
                {
                    txtHumedad.Text = linguistica[i];
                }
            }
            chart2.Series[4].Points.AddXY(hScrollBar2.Value, 0);
            chart2.Series[4].Points.AddXY(hScrollBar2.Value, 1);
        }
        #endregion ScrollBar2_Scroll

        #region chart3
        private void chart3_Click(object sender, EventArgs e)
        {

        }
        #endregion chart3

        #region ScrollBar3_Scroll
        private void hScrollBar3_Scroll(object sender, ScrollEventArgs e)
        {
            chart3.Series[4].Points.Clear();

            double x = double.Parse(hScrollBar3.Value.ToString());
            double[] Y = new double[4];
            Y[0] = p1.Graficar(x);
            Y[1] = p2.Graficar(x);
            Y[2] = p3.Graficar(x);
            Y[3] = p4.Graficar(x);

            String[] linguistica = { "Lenta", "Moderado", "Normal", "Fuerte" };

            double Max = Y.Max();

            for (int i = 0; i < Y.Length; i++)
            {
                if (Y[i] == Max)
                {
                    txtViento.Text = linguistica[i];
                }
            }
            chart3.Series[4].Points.AddXY(hScrollBar3.Value, 0);
            chart3.Series[4].Points.AddXY(hScrollBar3.Value, 1);
        }
        #endregion ScrollBar3_Scroll

        #region textBox1
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
           if (txtTemperatura.Equals("Baja") && txtHumedad.Equals("Poca") && txtViento.Equals("Lenta"))
            {
                textBox1.Text = "Clima Podria Llover";
            }
            //textBox1.Text = "Clima Podria Llover";
           // textBox1.SelectedText = "Clima Podria Llover";

        }
        #endregion textBox1

        
    }
}

